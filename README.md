# Corp Foundation libsass template

This is a template built by WATB (http://watb.co.uk) using Foundation v5.

## Demo!

Visit: http://watb.co.uk/corpdemo/

## Licence

This work is licensed under a Creative Commons Attribution 4.0 International (CC BY 4.0).
http://creativecommons.org/licenses/by/4.0/

You are free to use this template for any purpose but you must give appropriate credit. This exists in the form of a footer link to http://watb.co.uk which must not be removed.

## Requirements

You'll need to have the following items installed before you can work with this template.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone https://brightonmike@bitbucket.org/watb/corp-theme.git
npm install && bower install
```

While you're working on your project, run:

`grunt run`

When you're ready to deploy, you can create a production ready distribution of your site by running:

`grunt build`

## Directory Structure

  * `scss/_settings.scss`: Foundation configuration settings go in here
  * `scss/app.scss`: Application styles go here
