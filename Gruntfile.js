module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'expanded'
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }        
      }
    },

    browserSync: {
      bsFiles: {
          src : ['css/*.css', '*.html']
      },
      options: {
          watchTask: true,
          server: {
              baseDir: "./"
          }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 4 versions','ie 10','safari 6','safari 6.1','safari 7']
      },
      single_file: {
        options: {
          // Target-specific options go here.
        },
        src: 'css/app.css',
        dest: 'css/app.css'
      },      
    },  

    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [
        'bower_components/foundation/js/foundation/foundation.js',
        'bower_components/foundation/js/foundation/foundation.topbar.js',
        // 'bower_components/foundation/js/foundation.min.js',
        'js/map.js',
        'js/app.js'],
        //src: ['js/**/*.js'],
        dest: 'dist/js/build.js',
      },
      jquery: {
        src: ['bower_components/jquery/dist/jquery.js'],
        dest: 'dist/js/jquery.js',   
      },
      infobox: {
        src: ['js/infobox.js'],
        dest: 'dist/js/infobox.js',        
      }
    }, 

    uglify: {
      options: {
        banner: '/*! watb.co.uk michael gunner <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        src: ['dist/js/build.js'],
        dest: 'dist/js/build.min.js',
      },
      infobox: {
        src: ['dist/js/infobox.js'],
        dest: 'dist/js/infobox.min.js',
      },
      jquery: {
        src: ['dist/js/jquery.js'],
        dest: 'dist/js/jquery.min.js',
      }        
    }, 

    uncss: {
      dist: {
        options: {
          ignore: [
          '/meta/',
          '.button__reveal',
          '.open .content__video iframe',
          '.content__video',
          '.open .content__video',
          '.top-bar.expanded',
          '.top-bar.expanded .title-area',
          '.top-bar.expanded .toggle-topbar a',
          '.top-bar.expanded .toggle-topbar a span::after',
          'li.title.back.js-generated',
          new RegExp('^meta\..*'),
          new RegExp('/.top-bar.expanded/'),
          new RegExp('.*\.is-.*'),
          new RegExp('.*\.hide-for-.*'),
          new RegExp('.*\.has-.*')
          ]
        },        
        files: {
          'dist/css/tidy.css': ['index.html', 'about.html', 'contact.html', 'post.html', 'page.html', 'news.html', 'services.html']
        }
      }
    },

    processhtml: {
      dist: {
        files: {
          'dist/index.html': ['index.html'],
          'dist/about.html': ['about.html'],
          'dist/contact.html': ['contact.html'],
          'dist/post.html': ['post.html'],
          'dist/page.html': ['page.html'],
          'dist/news.html': ['news.html'],
          'dist/services.html': ['services.html']
        }
      }
    },

    imagemin: {                 
      dynamic: {               
        files: [{
          expand: true,               
          cwd: 'images/',                  
          src: ['**/*.{png,jpg,gif}'],  
          dest: 'dist/images'                 
        }]
      }
    },


    watch: {
      grunt: { files: ['Gruntfile.js'] },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  grunt.registerTask('build', ['sass','autoprefixer','concat','uglify','uncss','processhtml', 'imagemin']);

  grunt.registerTask('run', ['sass','autoprefixer','browserSync', 'watch']);

  grunt.registerTask('default', ['build','watch']);
}